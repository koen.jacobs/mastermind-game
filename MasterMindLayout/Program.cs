﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastermind
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variabelen
            Random random = new Random();   //De random generator.

            int aantalGokken = 0;           //Deze teller houdt bij hoeveel keer er al gegokt is.
            int maxAantalGokken = 2;       //Het maximale aantal gokken voordat het spel eindigt.
            int[] gok = new int[4];         //Deze array gevat de gok van de gebruiker.
            int[] juisteCode = new int[4];  //Deze array gevat de correcte code.

            int aantalJuist;
            int aantalVerkeerd;
            List<int> verkeerdeGevonden = new List<int>();
            List<int> juisteGevonden = new List<int>();
            List<int> juisteCodeLijst = new List<int>();

            //Genereer een willekeurige code.
            juisteCode[0] = random.Next(1, 7); juisteCode[1] = random.Next(1, 7); juisteCode[2] = random.Next(1, 7); juisteCode[3] = random.Next(1, 7);

            //Console.WriteLine("De juiste code, om eens te spieken bij het testen...:");
            //Print(juisteCode); Console.Write("\n");
            Console.WriteLine("We spelen Mastermind. Succes...");

            bool geraden = false;           //Deze boolean zal bijhouden of de juiste code is geraden.
            Console.WriteLine("Elke gok wordt gemaakt door door vier cijfers tussen 1 en 6 in te geven met spaties tussen. Bijvoorbeeld: 1 1 1 1");
            while (!geraden && aantalGokken < maxAantalGokken)//Zo lang de code niet geraden is EN de gebruiker nog niet aan het maximum aantal gokken zit gaat het spel door.
            {
                geraden = true;         //Deze waarde wordt later weer false als de gok niet klopt.
                aantalJuist = 0;        //Het aantal getallen in de gok die op de juiste plaats staan. Het aantal getallen die wel in de code voorkomen, maar op de verkeerde plaats worden later berekend                
                verkeerdeGevonden = new List<int>();
                juisteGevonden = new List<int>();
                aantalGokken++;
                Console.WriteLine($"Geef gok {aantalGokken} in.");
                String[] input = Console.ReadLine().Split(' '); ;
                for (int i = 0; i < 4; i++)
                {
                    gok[i] = Convert.ToInt32(input[i]);
                }
                for (int i = 0; i < 4; i++)
                {
                    if (gok[i] != juisteCode[i])//Dit gebeurt als het i'de cijfer van de gok niet klopt.
                    {
                        geraden = false;//Als er 1 getal verkeerd is kan de gok al niet kloppen.
                        bool gevonden = false;//We gaan checken of dit cijfer van de gok wel ergens anders in de code voorkomt. We starten met de veronderstelling dat dit niet zo is.
                        for (int j = 0; j < 4 && !gevonden; j++)//Zo lang we geen gelijke gevonden hebben blijven we de code checken of er een gelijk getal in komt.
                        {
                            if (gok[i] == juisteCode[j])
                            {
                                verkeerdeGevonden.Add(gok[i]);
                                gevonden = true;//Er is een ander getal in de code dat wel overeenkomt met deze gok.
                            }
                        }
                    }
                    else
                    {
                        juisteGevonden.Add(gok[i]);
                        aantalJuist++;
                    }
                }

                //Deze code gaat na welke van de getallen die wel in de code maar op de verkeerde plaats staan effectief meetellen.
                juisteCodeLijst = juisteCode.ToList();//Zet de getallen van de juiste code in een lijst, omdat ik te lui ben om hier met een array te werken.

                foreach (int juist in juisteGevonden)
                {
                    juisteCodeLijst.Remove(juist);
                }
                aantalVerkeerd = 0;
                foreach (int i in verkeerdeGevonden)
                {
                    if (juisteCodeLijst.Contains(i))
                    {
                        juisteCodeLijst.Remove(i);
                        aantalVerkeerd++;
                    }
                }

                //Console.WriteLine($"Er staan {aantalJuist} getallen juist, {aantalVerkeerd} getallen komen wel in de code voor, maar staan niet op hun plaats.");

                Print(gok);

                for (int i = 0; i < aantalJuist; i++)               //Zet voor elk juiste getal een grijze 'pion' achter de gok.
                {
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.Write(" ");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write(" ");
                }

                for (int i = 0; i < aantalVerkeerd; i++)            //Zet voor elk verkeerde getal dat wel in de code zit een witte 'pion' achter de gok.
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(" ");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write(" ");
                }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write("\n");
            }

            if (geraden)
            {
                Console.WriteLine("Gefeliciteerd!");
                Print(gok);
            }
            else
            {
                Console.WriteLine($"Jammer, je zetten zijn op. De juiste code was: ");
                Print(juisteCode);
            }

            Console.Read();
        }

        public static void Print(int[] code)
        {
            foreach (int i in code)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("|");
                switch (i)
                {
                    case 1:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"{i}");
                        break;
                    case 2:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write($"{i}");
                        break;
                    case 3:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.Write($"{i}");
                        break;
                    case 4:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($"{i}");
                        break;
                    case 5:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write($"{i}");
                        break;
                    case 6:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write($"{i}");
                        break;
                }
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("| ");
        }
    }
}
